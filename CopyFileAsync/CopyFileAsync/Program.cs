﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileUpper
{
    class Program
    {
        static void Main(string[] args)
        {
            var contexts = new List<AsyncTask>()
            {
                AsyncTask.Run(() => CopyFileAsync("Resources/file1.txt", "Resources/file1_copy.txt")),
                AsyncTask.Run(() => CopyFileAsync("Resources/file2.txt", "Resources/file2_copy.txt")),
                AsyncTask.Run(() => CopyFileAsync("Resources/file3.txt", "Resources/file3_copy.txt")),
            };

            while (contexts.Any(at => at.Done == false))
            {
                foreach (var elt_task in contexts)
                {
                    if (elt_task.CanMoveNext())
                    {
                        elt_task.MoveNext();
                    }
                }
            }
        }

        static IEnumerable<IAsyncStep> CopyFileAsync(string origin, string destination)
        {
            Console.WriteLine(String.Format("{0} - Read", origin));
            var readtask = ReadFileAsync(origin);
            yield return readtask;

            Console.WriteLine(String.Format("{0} - Write", origin));
            var file_content = readtask.Value;
            yield return WriteFileAsync(destination, file_content);

            Console.WriteLine(String.Format("{0} -  Work finished", origin));
            yield break;
        }

        static AsyncStep<byte[]> ReadFileAsync(string origine)
        {
            var asyncTask = new AsyncStep<byte[]>() { State = AsyncState.Await };

            var file = File.OpenRead(origine);
            var data = new byte[file.Length];
                
            asyncTask.Value = data;
            file.BeginRead(data, 0, (int) file.Length, (a) =>
            {
                file.Close();
                asyncTask.State = AsyncState.Continue;
            }, null);

            return asyncTask;
        }

        static AsyncStep WriteFileAsync(string destination, byte[] data)
        {
            var asyncTask = new AsyncStep() { State = AsyncState.Await };

            var file = File.OpenWrite(destination);
            file.BeginWrite(data, 0, data.Length, a => {
                file.Close();
                asyncTask.State = AsyncState.Continue;
            }, null);
            

            return asyncTask;
        }
    }
}
