﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileUpper
{
    public class AsyncTask
    {
        private IEnumerator<IAsyncStep> _stateMachine;
        private IAsyncStep CurrentTask;
        private bool _lastElement;

        public AsyncTask()
        {
            _lastElement = false;
        }

        public bool Done { get; private set; }

        public static AsyncTask Run(Func<IEnumerable<IAsyncStep>> func)
        {
            var task = new AsyncTask();
            task.Execute(func);
            return task;
        }

        public void Execute(Func<IEnumerable<IAsyncStep>> func)
        {
            this._stateMachine = func().GetEnumerator();
        }

        public bool CanMoveNext()
        {
            return this._stateMachine.Current == null ||
                (_lastElement == false && this._stateMachine.Current.State == AsyncState.Continue);
        }

        public void MoveNext()
        {
            Done = !this._stateMachine.MoveNext();
        }
    }
}
