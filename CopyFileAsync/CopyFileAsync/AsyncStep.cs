﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileUpper
{
    public enum AsyncState
    {
        Await,
        Continue
    }

    public interface IAsyncStep
    {
        AsyncState State { get; set; }
    }

    public class AsyncStep : IAsyncStep
    {
        public AsyncState State { get; set; }
        public object Value { get; set; }
    }

    public class AsyncStep<T> : IAsyncStep
    {
        public AsyncState State { get; set; }
        public T Value { get; set; }
    }
}
